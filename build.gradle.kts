import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    kotlin("jvm") version "1.5.10"
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

group = "nb.util"
version = "1.1-0"

repositories {
    jcenter()
}

fun DependencyHandlerScope.localImpl(fileName: String, path: String = "lib/") = implementation(files("$path$fileName"))

fun DependencyHandlerScope.localJar(fileName: String, path: String = "lib/") = localImpl("$fileName.jar", path)

dependencies {
    localJar("arguments-1.0-0")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
    kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlin.experimental.ExperimentalTypeInference"
    kotlinOptions.freeCompilerArgs += "-Xjvm-default=all"
    kotlinOptions.jvmTarget = "11"
}

tasks.withType<ShadowJar> {
    archiveBaseName.set("producerUtil")
    archiveVersion.set("1.1-0")
    minimize()
    dependencies {
        exclude(dependency(".*:.*:.*"))
    }
}
