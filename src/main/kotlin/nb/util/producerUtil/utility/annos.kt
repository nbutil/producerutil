package nb.util.producerUtil.utility

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.BINARY)
@RequiresOptIn("This is not yet stable, use at your own risk", RequiresOptIn.Level.WARNING)
annotation class Unstable