package nb.util.producerUtil.producer

import nb.util.arguments.Arguments
import nb.util.producerUtil.utility.Unstable
import kotlin.properties.Delegates

@Unstable
class ProducerBuilder<T> {

    val parameterTypes: MutableMap<String, Producer<T>> = mutableMapOf()

    fun addParameter(paramName: String, producer: Producer<T>): ProducerBuilder<T> {
        parameterTypes.computeIfAbsent(paramName) { producer }
        return this
    }

    inner class MapWrapper private constructor(private val map: MutableMap<String, Producer<T>> = parameterTypes) {
        constructor(): this(parameterTypes)
        fun <T> produceFrom(paramName: String): T = getParam<T>(paramName).produce().valueProduced
        fun <T> getParam(paramName: String): Producer<T> = map[paramName]!! as Producer<T>
    }

    var produceValue: MapWrapper.() -> T by Delegates.notNull()

    fun producerConstructor(block: MapWrapper.() -> T): ProducerBuilder<T> {
        produceValue = block
        return this
    }

    fun build(): Producer<T> = object: Producer<T> {
        private val producers = MapWrapper()
        override fun produce(arguments: Arguments): ProducerResult<T> = ProducerResultImpl(producers.produceValue())
    }
}
