package nb.util.producerUtil.producer

import nb.util.arguments.ArgumentKey
import nb.util.arguments.Arguments

interface Producer<T> {
    fun produce(arguments: Arguments = Arguments.EMPTY): ProducerResult<T>

    fun produce(vararg arguments: Pair<ArgumentKey<*>, Any?>): ProducerResult<T> {
        val args = Arguments(arguments = arguments)
        return this.produce(arguments = args)
    }

    fun produceRaw(arguments: Arguments = Arguments.EMPTY): T {
        val producedResult = produce(arguments)
        return producedResult.valueProduced
    }

    fun produceRaw(vararg arguments: Pair<ArgumentKey<*>, Any?>): T {
        val args = Arguments(arguments = arguments)
        return this.produceRaw(arguments = args)
    }
}
