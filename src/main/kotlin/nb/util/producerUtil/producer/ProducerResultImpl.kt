package nb.util.producerUtil.producer

import nb.util.arguments.Arguments

class ProducerResultImpl<T>(override val valueProduced: T, override val argumentsUsed: Arguments = Arguments.EMPTY): ProducerResult<T>
