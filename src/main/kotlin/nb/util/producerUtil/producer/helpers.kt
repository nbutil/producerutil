package nb.util.producerUtil.producer

import nb.util.producerUtil.utility.Unstable

@Unstable
fun <T> buildProducer(
    @BuilderInference block: ProducerBuilder<T>.() -> Unit
): Producer<T> {
    val builder = ProducerBuilder<T>()
    builder.apply(block)
    val builtProducer = builder.build()
    return builtProducer
}
