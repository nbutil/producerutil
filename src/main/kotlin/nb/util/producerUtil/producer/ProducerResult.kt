package nb.util.producerUtil.producer

import nb.util.arguments.Arguments


interface ProducerResult<T> {
    val valueProduced: T

    val argumentsUsed: Arguments
}
